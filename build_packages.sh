#!/bin/bash

#******************************************************************
#**
#**  custom build script for cmake
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# what are we building
PACKAGE_BASE=cmake
PACKAGE_VER=2.8.3
DEBIAN_VER=1~maverick1
TAR_STEM=${PACKAGE_BASE}-${PACKAGE_VER}
BUILD_VER=${PACKAGE_VER}-${DEBIAN_VER}
DSC_FILE=${PACKAGE_BASE}_${BUILD_VER}.dsc

# check binaries needed
binaries="dpkg rm dpkg-source dpkg-buildpackage"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# what architecture are we building for
echo -n "Checking build architecture... "
BUILD_ARCH=`dpkg --print-architecture`
if [ -z "${BUILD_ARCH}" ]
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

# need the arch to determine this
CHANGES_FILE=${PACKAGE_BASE}_${BUILD_VER}_${BUILD_ARCH}.changes

echo -n "Extracting source... "
if ! dpkg-source -x ${DSC_FILE}
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

echo -n "Changing to source dir... "
if ! pushd ${TAR_STEM} > /dev/null
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

echo "Building package..."
if ! dpkg-buildpackage -b
then
	echo "BUILD FAILED!"
	exit 1
else
	echo "Build succeeded."
fi

echo -n "Popping out of build directory... "
if ! popd > /dev/null
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

echo -n "Removing build dir... "
if ! rm -rf ${TAR_STEM}
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

echo -n "Removing unnecessary build files... "
if ! rm ${CHANGES_FILE}
then
	echo "FAIL"
	exit 1
else
	echo "OK"
fi

exit 0
