Format: 3.0 (quilt)
Source: cmake
Binary: cmake, cmake-data, cmake-curses-gui, cmake-qt-gui, cmake-doc, cmake-dbg
Architecture: any
Version: 2.8.3-1~maverick1
Maintainer: Modestas Vainius <modax@debian.org>
Uploaders: A. Maitland Bottoms <bottoms@debian.org>, Kai Wasserbäch <debian@carbon-project.org>
Homepage: http://cmake.org/
Standards-Version: 3.9.1
Vcs-Browser: http://git.debian.org/?p=collab-maint/cmake.git;a=summary
Vcs-Git: git://git.debian.org/git/collab-maint/cmake.git
Build-Depends: debhelper (>= 7.3), libcurl4-gnutls-dev, libxmlrpc-c3-dev, libexpat1-dev, zlib1g-dev, libncurses5-dev, libqt4-dev (>= 4.4.0), procps [!hurd-any], libarchive-dev (>= 2.8.0)
Checksums-Sha1: 
 e9bfbec47b3939be6f83ffa8006884285c687dc2 5436543 cmake_2.8.3.orig.tar.gz
 b2ed24edd7e65d92d1b6dca236006744916b7185 21988 cmake_2.8.3-1~maverick1.debian.tar.gz
Checksums-Sha256: 
 689ed02786b5cefa5515c7716784ee82a82e8ece6be5a3d629ac3cc0c05fc288 5436543 cmake_2.8.3.orig.tar.gz
 215224570b4c0cc81ee7a6a9115440e7ab98e3880228bcd2e40c2f5bc0039203 21988 cmake_2.8.3-1~maverick1.debian.tar.gz
Files: 
 a76a44b93acf5e3badda9de111385921 5436543 cmake_2.8.3.orig.tar.gz
 bb23543091adb1074c7633fe228e76d4 21988 cmake_2.8.3-1~maverick1.debian.tar.gz
